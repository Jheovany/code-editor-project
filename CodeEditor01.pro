#-------------------------------------------------
#
# Project created by QtCreator 2016-10-13T12:40:17
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CodeEditor01
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp\
        codeeditor.cpp\
        highlighter.cpp \
    splitterwidget.cpp

HEADERS  += mainwindow.h\
            codeeditor.h\
            highlighter.h \
            tabeditor.h \
    splitterwidget.h

FORMS    += mainwindow.ui

RESOURCES += \
    recursos.qrc

DISTFILES +=

