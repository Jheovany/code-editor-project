#include <QtWidgets>

#include "codeeditor.h"
#include <QCompleter>
#include <QKeyEvent>
#include <QAbstractItemView>
#include <QtDebug>
#include <QApplication>
#include <QModelIndex>
#include <QAbstractItemModel>
#include <QScrollBar>

CodeEditor::CodeEditor(QWidget *parent) : QPlainTextEdit(parent)
{
    qcompleter=0;
    wordNotFound = "";
    haveCompleter=false;
    lineNumberArea = new LineNumberArea(this);

    connect(this, SIGNAL(blockCountChanged(int)), this, SLOT(updateLineNumberAreaWidth(int)));
    connect(this, SIGNAL(updateRequest(QRect,int)), this, SLOT(updateLineNumberArea(QRect,int)));
    connect(this, SIGNAL(cursorPositionChanged()), this, SLOT(highlightCurrentLine()));

    updateLineNumberAreaWidth(0);
    exist=false;
    save=true;

    fontName = "Monaco";

    _font = QFont(fontName, 14);
    _font.setStyleHint(QFont::Monospace);
    _font.setFixedPitch(true);
    setFont(_font);
    setTabStopWidth(4 * fontMetrics().width(' '));
    setLineWrapMode(LineWrapMode::WidgetWidth);
    new Highlighter(document());
    setPlainText("// New source code");
    setTabChangesFocus(false);
}
void CodeEditor::desidentar(){
    QTextCursor cur = textCursor();
    int a = cur.anchor();
    int p = cur.position();
    int start = (a<=p?a:p);
    int end = (a>p?a:p);

    cur.beginEditBlock();
    cur.setPosition(end);
    int eblock = cur.block().blockNumber();
    cur.setPosition(start);
    int sblock = cur.block().blockNumber();
    QString s;

    for(int i = sblock; i <= eblock; i++)
    {
        cur.movePosition(QTextCursor::EndOfBlock, QTextCursor::MoveAnchor);
        cur.movePosition(QTextCursor::StartOfBlock, QTextCursor::KeepAnchor);
        s = cur.selectedText();
        if(!s.isEmpty()){
            if(s.startsWith("    ") || s.startsWith("   \t")){
                cur.movePosition(QTextCursor::StartOfBlock, QTextCursor::MoveAnchor);
                cur.movePosition(QTextCursor::NextCharacter, QTextCursor::KeepAnchor, 4);
                cur.removeSelectedText();
            }else if(s.startsWith("   ") || s.startsWith("  \t")){
                cur.movePosition(QTextCursor::StartOfBlock, QTextCursor::MoveAnchor);
                cur.movePosition(QTextCursor::NextCharacter, QTextCursor::KeepAnchor, 3);
                cur.removeSelectedText();
            }else if(s.startsWith("  ") || s.startsWith(" \t")){
                cur.movePosition(QTextCursor::StartOfBlock, QTextCursor::MoveAnchor);
                cur.movePosition(QTextCursor::NextCharacter, QTextCursor::KeepAnchor, 2);
                cur.removeSelectedText();
            }else if(s.startsWith(" ") || s.startsWith("\t")){
                cur.movePosition(QTextCursor::StartOfBlock, QTextCursor::MoveAnchor);
                cur.movePosition(QTextCursor::NextCharacter, QTextCursor::KeepAnchor, 1);
                cur.removeSelectedText();
            }
        }
        cur.movePosition(QTextCursor::NextBlock, QTextCursor::MoveAnchor);
    }
    cur.endEditBlock();

}

bool CodeEditor::identar(){

    QTextCursor curs = textCursor();
    if(!curs.hasSelection())
        return false;
    int spos = curs.anchor(), epos = curs.position();
    if(spos > epos)
    {
        int hold = spos;
        spos = epos;
        epos = hold;
    }

    curs.setPosition(spos, QTextCursor::MoveAnchor);
    int sblock = curs.block().blockNumber();

    curs.setPosition(epos, QTextCursor::MoveAnchor);
    int eblock = curs.block().blockNumber();

    curs.setPosition(spos, QTextCursor::MoveAnchor);
    curs.beginEditBlock();

    for(int i = 0; i <= (eblock - sblock); ++i)
    {

        curs.movePosition(QTextCursor::StartOfBlock, QTextCursor::MoveAnchor);
        curs.insertText("\t");
        curs.movePosition(QTextCursor::NextBlock, QTextCursor::MoveAnchor);
    }

    curs.endEditBlock();
    curs.setPosition(spos, QTextCursor::MoveAnchor);
    curs.movePosition(QTextCursor::StartOfBlock, QTextCursor::MoveAnchor);

    while(curs.block().blockNumber() < eblock)
    {
        curs.movePosition(QTextCursor::NextBlock, QTextCursor::KeepAnchor);
    }
    curs.movePosition(QTextCursor::EndOfBlock, QTextCursor::KeepAnchor);

    setTextCursor(curs);
    return true;
}

void CodeEditor::setCompleter(QCompleter *completer)
{
    if (qcompleter)
        QObject::disconnect(qcompleter, 0, this, 0);

    qcompleter = completer;

    if (!qcompleter)
        return;

    qcompleter->setWidget(this);
    qcompleter->setCompletionMode(QCompleter::PopupCompletion);
    qcompleter->setCaseSensitivity(Qt::CaseInsensitive);
    QObject::connect(qcompleter, SIGNAL(activated(QString)),
                     this, SLOT(insertCompletion(QString)));
    haveCompleter=true;
}

QCompleter *CodeEditor::completer() const
{
    return qcompleter;
}

void CodeEditor::insertCompletion(const QString& completion)
{
    if (qcompleter->widget() != this)
        return;
    QTextCursor tc = textCursor();
    int extra = completion.length() - qcompleter->completionPrefix().length();
    tc.movePosition(QTextCursor::Left);
    tc.movePosition(QTextCursor::EndOfWord);
    tc.insertText(completion.right(extra));
    setTextCursor(tc);
}

QString CodeEditor::textUnderCursor() const
{
    QTextCursor tc = textCursor();
    tc.select(QTextCursor::WordUnderCursor);
    return tc.selectedText();
}

void CodeEditor::focusInEvent(QFocusEvent *e)
{
    QPlainTextEdit::focusInEvent(e);
}

void CodeEditor::keyPressEvent(QKeyEvent *e)
{
    if ((e->key() == Qt::Key_Return || e->key() == Qt::Key_Enter)  && !qcompleter->popup()->isVisible() ){
            QTextCursor cur = textCursor();
            cur.movePosition(QTextCursor::PreviousCharacter);
            cur.movePosition(QTextCursor::NextCharacter,QTextCursor::KeepAnchor);
            QString anterior = cur.selectedText();
            cur.clearSelection();
            cur.movePosition(QTextCursor::NextCharacter,QTextCursor::KeepAnchor);
            QString siguiente = cur.selectedText();
            cur = textCursor();
            QPlainTextEdit::keyPressEvent(e);
            cur.movePosition(QTextCursor::PreviousBlock);
            cur.movePosition(QTextCursor::StartOfBlock);
            cur.movePosition(QTextCursor::EndOfBlock, QTextCursor::KeepAnchor);
            QString str = cur.selectedText();
            QRegExp rx("^([\\t ]+)");
            if(str.indexOf(rx) >= 0) textCursor().insertText(rx.cap(1) );
            if(anterior=="{" && siguiente=="}"){
                QPlainTextEdit::keyPressEvent(e);
                if(str.indexOf(rx) >= 0) textCursor().insertText(rx.cap(1) );
                cur = textCursor();
                cur.movePosition(QTextCursor::PreviousBlock);
                cur.movePosition(QTextCursor::EndOfBlock);
                setTextCursor(cur);
                textCursor().insertText("\t");
            }
            if(anterior=="{" && siguiente!="}" || anterior==")") textCursor().insertText("\t");
            return;
    }
    switch(e->key())
    {
    case Qt::Key_Tab:
        if(identar()) return;
        break;
    case Qt::Key_Backtab:
        desidentar();
        break;
    }
    if(e->key()=='(' || e->key()=='[' || e->key()=='{'){
        QTextCursor tc = textCursor();
        QPlainTextEdit::keyPressEvent(e);
        insertPlainText( QChar((e->key()=='(')? e->key()+1:e->key()+2));
        tc.movePosition(QTextCursor::PreviousCharacter);
        setTextCursor(tc);
        if(qcompleter->popup()->isVisible()) qcompleter->popup()->hide();
        return;
    }
    if(e->key() == Qt::Key_Space && wordNotFound.size()>0){
        QStringListModel *model = (QStringListModel*)(qcompleter->model());
        QStringList stringList = model->stringList();
        stringList.append(wordNotFound);
        model->setStringList(stringList);
        wordNotFound = "";
    }
    if(haveCompleter){
        if (qcompleter && qcompleter->popup()->isVisible()) {
            // The following keys are forwarded by the completer to the widget
           switch (e->key()) {
           case Qt::Key_Enter:
           case Qt::Key_Return:
           case Qt::Key_Escape:
           case Qt::Key_Tab:
           case Qt::Key_Backtab:
                enter_pressed=false;
                e->ignore();
                return; // let the completer do default behavior
           default:
               break;
           }
        }


        bool isShortcut = ((e->modifiers() & Qt::ControlModifier) && e->key() == Qt::Key_E); // CTRL+E
        if (!qcompleter || !isShortcut) // do not process the shortcut when we have a completer
            QPlainTextEdit::keyPressEvent(e);

        const bool ctrlOrShift = e->modifiers() & (Qt::ControlModifier | Qt::ShiftModifier);
        if (!qcompleter || (ctrlOrShift && e->text().isEmpty()))
            return;

        static QString eow("~!@#$%^&*()_+{}|:\"<>?,./;'[]\\-="); // end of word
        bool hasModifier = (e->modifiers() != Qt::NoModifier) && !ctrlOrShift;
        QString completionPrefix = textUnderCursor();

        if (!isShortcut && (hasModifier || e->text().isEmpty()|| completionPrefix.length() < 3
                          || eow.contains(e->text().right(1)))) {
            qcompleter->popup()->hide();
            return;
        }

        if (completionPrefix != qcompleter->completionPrefix()) {
            qcompleter->setCompletionPrefix(completionPrefix);
            qcompleter->popup()->setCurrentIndex(qcompleter->completionModel()->index(0, 0));

        }
        QRect cr = cursorRect();
        cr.setX(cr.x()+lineNumberAreaWidth());
        cr.setWidth(qcompleter->popup()->sizeHintForColumn(0)
                    + qcompleter->popup()->verticalScrollBar()->sizeHint().width());
        //c->popup()->font().setPointSize(18);
        qcompleter->popup()->setFont(this->font());
        qcompleter->complete(cr); // popup it up!
        if(qcompleter->currentCompletion().size()==0 && completionPrefix.size()>3){
            wordNotFound = completionPrefix;
        }
    }else{
        QPlainTextEdit::keyPressEvent(e);
    }
}

bool CodeEditor::isExist(){return exist;}
void CodeEditor::setExist(bool state){exist=state;}
void CodeEditor::setSave(bool _save){save=_save;}
bool CodeEditor::isSave(){return save;}
int CodeEditor::lineNumberAreaWidth()
{
    int digits = 1;
    int max = qMax(1, blockCount());
    while (max >= 10) {
        max /= 10;
        ++digits;
    }

    int space = 24 + fontMetrics().width(QLatin1Char('9')) * digits;
    return space;
}

void CodeEditor::setFontSize(int size)
{
    _font.setPointSize(size);
    setFont(_font);
    setTabStopWidth(4 * fontMetrics().width(' '));

}

void CodeEditor::updateLineNumberAreaWidth(int /* newBlockCount */)
{
    setViewportMargins(lineNumberAreaWidth(), 0, 0, 0);
}


void CodeEditor::updateLineNumberArea(const QRect &rect, int dy)
{
    if (dy)
        lineNumberArea->scroll(0, dy);
    else
        lineNumberArea->update(0, rect.y(), lineNumberArea->width(), rect.height());

    if (rect.contains(viewport()->rect()))
        updateLineNumberAreaWidth(0);
}

QString CodeEditor::getFileName() const
{
    return fileName;
}

void CodeEditor::setFileName(const QString &value)
{
    fileName = value;
}


void CodeEditor::resizeEvent(QResizeEvent *e)
{
    QPlainTextEdit::resizeEvent(e);

    QRect cr = contentsRect();
    lineNumberArea->setGeometry(QRect(cr.left(), cr.top(), lineNumberAreaWidth(), cr.height()));
}


void CodeEditor::highlightCurrentLine()
{
    QList<QTextEdit::ExtraSelection> extraSelections;

    if (!isReadOnly()) {
        QTextEdit::ExtraSelection selection;

        QColor lineColor = QColor(Qt::cyan).lighter(160);

        selection.format.setBackground(lineColor);
        selection.format.setProperty(QTextFormat::FullWidthSelection, true);
        selection.cursor = textCursor();
        selection.cursor.clearSelection();
        extraSelections.append(selection);
    }

    setExtraSelections(extraSelections);
}


void CodeEditor::lineNumberAreaPaintEvent(QPaintEvent *event)
{
    QPainter painter(lineNumberArea);
    painter.setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);
    painter.fillRect(event->rect(), QColor("#f5f5f5"));
    QLine boderLeft(event->rect().width(), 0, event->rect().width(), event->rect().height());
    painter.setPen(QColor("#ccc"));
    painter.drawLine(boderLeft);

    QTextBlock block = firstVisibleBlock();
    int blockNumber = block.blockNumber();
    int top = (int) blockBoundingGeometry(block).translated(contentOffset()).top();
    int bottom = top + (int) blockBoundingRect(block).height();

    while (block.isValid() && top <= event->rect().bottom()) {
        if (block.isVisible() && bottom >= event->rect().top()) {
            QString number = QString::number(blockNumber + 1);
            painter.setPen(Qt::black);
            painter.drawText(0, top, lineNumberArea->width() - 8, fontMetrics().height(),
                             Qt::AlignRight, number);
        }

        block = block.next();
        top = bottom;
        bottom = top + (int) blockBoundingRect(block).height();
        ++blockNumber;
    }
}

