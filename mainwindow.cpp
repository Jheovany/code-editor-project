
#include <QtWidgets>
#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);
    ui->spinBox->setFocusPolicy(Qt::NoFocus);
    ui->txtRowPosition->setFocusPolicy(Qt::NoFocus);
    //tabWidget->setStyleSheet("QTabWidget::pane { border: 0; }");
    //setCentralWidget(tabWidget);
    //signalMapper = new QSignalMapper(this);

    //connect(signalMapper, SIGNAL(mapped(int)),this, SLOT(documentWasModified(int)));
    //tabWidget->setTabsClosable(true);
    //layout->addWidget(ui->plainTextEdit);
    //layout->setContentsMargins(0,30,0,0);
    //tabWidget->setLayout(layout);

    //tabWidget->addTab(new CodeEditor, "New...");


    //modelFromFile(":/resources/wordlist.txt");

    count_doc=1;

    QVBoxLayout *splitterLayout = new QVBoxLayout;
    splitterLayout->setContentsMargins(0, 0, 0, 0);
    ui->splitterWidget->setLayout(splitterLayout);
    splitterWidget = new SplitterWidget(this);
    tabWidget = splitterWidget->getTabWidget();
    splitterLayout->addWidget(splitterWidget);

    newFile();
    connect(ui->actionNew_file, &QAction::triggered, this, &MainWindow::newFile);
    connect(ui->actionSave_file,&QAction::triggered,this,&MainWindow::saveFile);
    connect(ui->actionSave_File_As,&QAction::triggered,this,&MainWindow::saveFileAs);
    connect(ui->actionFonts,&QAction::triggered,this,&MainWindow::selectFont);
    connect(tabWidget,SIGNAL(tabCloseRequested(int)),this,SLOT(tabToClose(int)));
    connect(splitterWidget->getTreeView(),SIGNAL(doubleClicked(QModelIndex)), this, SLOT(selectedFile(QModelIndex)));
}

void MainWindow::selectFont(){
    CodeEditor *editor = (CodeEditor*)tabWidget->currentWidget();
    if(editor){
        bool ok;
        QFont font = QFontDialog::getFont(&ok);
        if(ok)
            editor->setFont(font);
    }
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if (checkSaves()) {
        event->accept();
    } else {
        event->ignore();
    }
}


bool MainWindow::checkSaves(){
    for(int i=tabWidget->count()-1;i>=0;i--){
        tabWidget->setCurrentIndex(tabWidget->count()-1);
        CodeEditor *editor = (CodeEditor*)tabWidget->currentWidget();
        if(maybeSave()){
            tabWidget->removeTab(i);
            ui->txtRowPosition->setText("");
        }else{
            break;
        }
    }
    if(tabWidget->count()==0)
        return true;
    else
        return false;
}
void MainWindow::selectedFile(const QModelIndex index)
{

    QString title;
    QFile file(splitterWidget->getFileSystemModel()->filePath(index));
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) return;
    newFile();
    QTextStream in(&file);
    CodeEditor *editor = (CodeEditor*)tabWidget->currentWidget();
    editor->setExist(true);
    title = splitterWidget->getFileSystemModel()->fileName(index);
    editor->setPlainText(in.readAll());
    editor->setFileName(title);
    editor->setDocumentTitle(title);
    tabWidget->setTabText(tabWidget->currentIndex(),editor->documentTitle());
    editor->setSave(true);
    count_doc--;
}

QAbstractItemModel *MainWindow::modelFromFile(const QString& fileName)
{
    QFile file(fileName);
    if (!file.open(QFile::ReadOnly))
        return new QStringListModel(completer);

#ifndef QT_NO_CURSOR
    QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
#endif
    QStringList words;

    while (!file.atEnd()) {
        QByteArray line = file.readLine();
        if (!line.isEmpty())
            words << line.trimmed();
    }

#ifndef QT_NO_CURSOR
    QApplication::restoreOverrideCursor();
#endif
    return new QStringListModel(words, completer);
}


void MainWindow::newFile()
{

    QStringList stringList;
    stringList << "double" << "string" << "float";
    QStringListModel *model = new QStringListModel(stringList);
    completer = new QCompleter(model, this);
    CodeEditor* editor = new CodeEditor();
    completer->setCaseSensitivity(Qt::CaseInsensitive);
    completer->setWrapAround(false);
    editor->setDocumentTitle("New"+QString::number(count_doc)+"...");
    editor->setCompleter(completer);
    int p= tabWidget->addTab(editor,editor->documentTitle());
    tabWidget->setCurrentIndex(p);		//Set new current tab
    tabWidget->setCurrentWidget( editor);
    qDebug() << "tab " << p;
    ui->spinBox->setValue(editor->font().pointSize());
    connect(editor,SIGNAL(cursorPositionChanged()),this,SLOT(cursorChanged()));
    connect(editor->document(), SIGNAL(contentsChanged()),this,SLOT(documentWasModified()));
    cursorChanged();
    count_doc++;
}

void MainWindow::cursorChanged(){
    CodeEditor *editor = (CodeEditor*)tabWidget->currentWidget();
    if(editor){
        QTextCursor cursor = editor->textCursor();
        int x = cursor.columnNumber() + 1;
        ui->txtRowPosition->setText(QString::number(x));
    }
}

void MainWindow::tabToClose(int p){
    CodeEditor *editor = (CodeEditor*)tabWidget->currentWidget();
    if(maybeSave()){
        tabWidget->removeTab(p);
        ui->txtRowPosition->setText("");
    }
}

bool MainWindow::maybeSave()
{
    CodeEditor *editor = (CodeEditor*)tabWidget->currentWidget();
    QString msg = "The document ["+editor->documentTitle()+"] has been modified.\n"
                                                           "Do you want to save your changes?";
    if (editor->isSave())
        return true;
    const QMessageBox::StandardButton ret
        = QMessageBox::warning(this, tr("CodeEditor"),
                               msg,
                               QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
    switch (ret) {
    case QMessageBox::Save:
        saveFile();
        break;
    case QMessageBox::Cancel:
        return false;
    default:
        break;
    }
    return true;
}


void MainWindow::saveFile()
{
    CodeEditor *editor = (CodeEditor*)tabWidget->currentWidget();
    if(editor->isExist()){
        save(editor);
    }else{
        saveAs(editor);
    }
    tabWidget->setTabText(tabWidget->currentIndex(),editor->documentTitle());

}

void MainWindow::save(CodeEditor *e)
{
    QFile file(e->getFileName());
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) return;
    QTextStream out(&file);
    out << e->toPlainText();
    e->setExist(true);
    e->setSave(true);
}

void MainWindow::saveAs(CodeEditor *e)
{
    QFileDialog dialog(this);
    dialog.setWindowModality(Qt::WindowModal);
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    if (dialog.exec() != QDialog::Accepted)
        return;
    e->setFileName(dialog.selectedFiles().first());
    e->setDocumentTitle(QFileInfo(dialog.selectedFiles().at(0)).fileName());
    save(e);
}

void MainWindow::documentWasModified()
{
    CodeEditor *editor = (CodeEditor*)tabWidget->currentWidget();
    if(editor){
        tabWidget->setTabText(tabWidget->currentIndex(),editor->documentTitle()+"*");
        editor->setSave(false);
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_spinBox_valueChanged(int arg1)
{
    CodeEditor *editor = (CodeEditor*)tabWidget->currentWidget();
    if(editor){
        editor->setFontSize(arg1);
    }
}

void MainWindow::saveFileAs(){

    CodeEditor *editor = (CodeEditor*)tabWidget->currentWidget();
    saveAs(editor);
    tabWidget->setTabText(tabWidget->currentIndex(),editor->documentTitle());
}

void MainWindow::on_tabWidget_currentChanged(int index)
{
    CodeEditor *editor = (CodeEditor*)tabWidget->currentWidget();
    if(editor){
        ui->spinBox->setValue(editor->font().pointSize());
        editor->ensureCursorVisible() ;
    }
}

void MainWindow::on_actionOpen_file_triggered()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::ExistingFiles);
    QStringList fileNames;
    QString title="";
    int i=0;

    if (dialog.exec()){
        fileNames = dialog.selectedFiles();

        int tabIndex;
        for (i = 0; i < fileNames.length(); i++) {
            QFile file(fileNames.at(i));
            if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) return;
            newFile();
            QTextStream in(&file);
            CodeEditor *editor = (CodeEditor*)tabWidget->currentWidget();
            editor->setExist(true);
            title = QFileInfo(fileNames.at(i)).fileName();
            editor->setPlainText(in.readAll());
            editor->setFileName(fileNames.at(i));
            editor->setDocumentTitle(title);
            tabWidget->setTabText(tabWidget->currentIndex(),editor->documentTitle());
            editor->setSave(true);
            count_doc--;
        }
    }
}


void MainWindow::on_actionFolder_Up_triggered()
{
    splitterWidget->cdUp();
}

void MainWindow::on_actionZoom_triggered()
{
    if(!splitterWidget->getTreeView()->isHidden()){
        splitterWidget->getTreeView()->hide();
        ui->actionZoom->setIcon(QIcon(":/resources/ic_full.png"));
    }else{
        splitterWidget->getTreeView()->show();
        ui->actionZoom->setIcon(QIcon(":/resources/ic_exit.png"));
    }
}

void MainWindow::on_actionFolder_Select_triggered()
{
    QModelIndex index =splitterWidget->getTreeView()->currentIndex();
    QFileInfo info = splitterWidget->getFileSystemModel()->fileInfo(index);
    QString dir=info.absoluteDir().absolutePath();
    dir += ((info.isDir())? ("/"+info.baseName()):"");
    if(dir.size()>0){
        splitterWidget->getFileSystemModel()->setRootPath(dir);
        splitterWidget->getTreeView()->setRootIndex(splitterWidget->getFileSystemModel()->index(dir));
    }
}
