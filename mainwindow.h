#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVBoxLayout>
#include <QSignalMapper>
#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>
#include <QTextCursor>
#include "codeeditor.h"
#include "splitterwidget.h"

QT_BEGIN_NAMESPACE
class QAbstractItemModel;
class QComboBox;
class QCompleter;
class QLabel;
class QLineEdit;
class QProgressBar;
QT_END_NAMESPACE

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
protected:
    void closeEvent(QCloseEvent *event) Q_DECL_OVERRIDE;
private slots:
    void tabToClose(int);
    void on_spinBox_valueChanged(int arg1);
    void selectFont();
    void newFile();
    void saveFile();
    void saveFileAs();
    void documentWasModified();
    void on_tabWidget_currentChanged(int index);
    void on_actionOpen_file_triggered();
    void cursorChanged();
    void selectedFile(const QModelIndex index);
    void on_actionFolder_Up_triggered();

    void on_actionZoom_triggered();

    void on_actionFolder_Select_triggered();

private:
    bool maybeSave();
    void saveAs(CodeEditor*);
    void save(CodeEditor*);
    bool checkSaves();

    int count_doc;
    Ui::MainWindow *ui;
    QSignalMapper *signalMapper;
    QAbstractItemModel *modelFromFile(const QString& fileName);
    QCompleter *completer;
    QTabWidget *tabWidget;
    SplitterWidget *splitterWidget;
};

#endif // MAINWINDOW_H
