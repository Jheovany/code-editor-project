#ifndef TABEDITOR_H
#define TABEDITOR_H

#include <QTabWidget>

class TabEditor: public QTabWidget
{
  Q_OBJECT
  public:
    TabEditor(QWidget* parent)
    {
        this->setParent(parent);
        //connect(this,SIGNAL(tabCloseRequested(int)),this,SLOT(closeTab(int)));
    };
    ~ TabEditor(){};

   public slots:
    void closeTab(int index)
    {
        this->removeTab(index);
    };
};

#endif // TABEDITOR_H
