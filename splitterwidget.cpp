#include "splitterwidget.h"

#include <QDebug>

SplitterWidget::SplitterWidget(QWidget *parent) : QSplitter(parent)
{
    this->setFocusPolicy(Qt::NoFocus);
    treeView = new QTreeView;
    treeView->setFocusPolicy(Qt::NoFocus);
    fileModel = new QFileSystemModel(this);
    fileModel->setRootPath(QDir::currentPath());
    treeView->setModel(fileModel);
    treeView->setRootIndex(fileModel->index(QDir::currentPath()));
    treeView->setColumnWidth(0, 200);
    treeView->setColumnHidden(0,false);
    treeView->setColumnHidden(1,true);
    treeView->setColumnHidden(2,true);
    treeView->setColumnHidden(3,false);
    tabWidget = new QTabWidget;
    tabWidget->setTabsClosable(true);
    addWidget(treeView);
    addWidget(tabWidget);
    //tabWidget->addTab(new QWidget, "Tab 1");
    setStretchFactor(1, 1);
    selectionModel = treeView->selectionModel();
    connect(selectionModel, SIGNAL(selectionChanged(const QItemSelection&,const QItemSelection&)), this, SLOT(mySelectionChanged(const QItemSelection&,const QItemSelection&)));

}


void SplitterWidget::mySelectionChanged(const QItemSelection&,const QItemSelection&){
    qDebug() << "nueva ruta ";
}

void SplitterWidget::cdUp()
{
    QDir dir = fileModel->rootDirectory();
    dir.cdUp();
    fileModel->setRootPath(dir.absolutePath());
    treeView->setRootIndex(fileModel->index(dir.absolutePath()));
    qDebug() << dir.absolutePath();
}

QTabWidget *SplitterWidget::getTabWidget() const
{
    return tabWidget;
}

